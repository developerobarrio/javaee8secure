

function cargaDatos( columns , url  ) {
    $.ajax({
        url: "http://localhost:8080/krieg/rest/imperio"
    }).then(function(data) {
        let role = data;
        $("#imperio").text(role.toUpperCase());
        let furl = url  + role;
        loadTable(furl,columns);
    });
}

function loadTable(url,columns) {


    var body = $("#tabla").find('tbody');

    $.getJSON(url, function (result) {
        $.each(result, function (i, json) {
            createRow(json);
        });
    }).fail(function(jqXHR, textStatus, errorThrown ) {
      var httpCode = jqXHR.status;
      if (httpCode == 403) {
	      spy();
      }
    });

    function spy(){
       document.location = "traidor.html"
    }

    function createRow(json) {

        function createColumn(x) {
            var column = $('<td>');
            return column.append(x);
        }

        var row = $('<tr>');
        columns.map(s => "json." + s)
                     .map(x => eval(x))
                    .map(x => createColumn(x))
                    .map(x => row.append(x));
        body.append(row);
    }
}