package es.nacho.javaee8.security.domain.krieg;

import javax.persistence.*;

@Entity
@Table(name = "COHORTE")
@NamedQuery(name = Cohorte.FIND_ALL , query = "select c from Cohorte c")
@NamedQuery(name = Cohorte.FIND_ROMA , query = "select L.cohorte from  Legion L where   L.imperio = es.nacho.javaee8.security.domain.krieg.Imperio.ROMA" )
@NamedQuery(name = Cohorte.FIND_CARTAGO , query = "select L.cohorte from  Legion L where   L.imperio = es.nacho.javaee8.security.domain.krieg.Imperio.ROMA" )
public class Cohorte {

    public  static final String  FIND_ALL = "cohorte.findAll";
    public  static final String  FIND_ROMA = "cohorte.findRoma";
    public  static final String  FIND_CARTAGO = "cohorte.findCartago";


    @Id
    private String id;

    @Enumerated(EnumType.STRING)
    private Tipo tipo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
}
