package es.nacho.javaee8.security.domain.krieg;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;

@Entity
@Table(name = "LEGADO")
@NamedQuery(name = Legado.FIND_ALL , query = "select c from Legado c")
//@NamedQuery(name = Legado.FIND_ROMA, query = "select c from Legado c where c.legion.imperio =  es.nacho.javaee8.security.domain.krieg.Imperio.ROMA")
//@NamedQuery(name = Legado.FIND_CARTAGO,query = "select c from Legado c where c.legion.imperio =  es.nacho.javaee8.security.domain.krieg.Imperio.CARTAGO")


public class Legado {

    public  static final String FIND_ALL = "legado.findAll";
  //  public  static final String FIND_ROMA= "legado.findRoma";
  //  public  static final String FIND_CARTAGO= "legado.findCartago";

    @Id   private String id;
    @OneToOne
    @JsonbTransient
    private Legion legion;

    private String nombre;

    public Legion getLegion() {
        return legion;
    }

    public void setLegion(Legion legion) {
        this.legion = legion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}

