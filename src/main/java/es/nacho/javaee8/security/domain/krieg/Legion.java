package es.nacho.javaee8.security.domain.krieg;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NamedQuery(name = Legion.FIND_ALL , query = "select c from Legion c")
@NamedQuery(name = Legion.ROMANA , query = "select c from Legion c where  c.imperio = es.nacho.javaee8.security.domain.krieg.Imperio.ROMA")
@NamedQuery(name = Legion.CARTAGINESA , query = "select c from Legion c where  c.imperio = es.nacho.javaee8.security.domain.krieg.Imperio.CARTAGO")
@Table(name= "LEGION")
public class Legion implements Serializable {

    public static final String FIND_ALL = "legion.findAll";
    public static final String ROMANA = "legion.romana";
    public static final String CARTAGINESA = "legion.cartaginesa";

    @Id   private String id;

    @OneToOne(mappedBy = "legion",fetch = FetchType.LAZY)    private  Legado legado;

    @OneToMany( cascade = CascadeType.ALL, orphanRemoval = true ,fetch = FetchType.EAGER)
    @JoinTable(name="LEGION_COHORTE",
            joinColumns=@JoinColumn(name="LEGION_ID"),
            inverseJoinColumns=@JoinColumn(name="COHORTE_ID"))
    private List<Cohorte> cohorte;

    @Enumerated(EnumType.STRING)
    private Imperio imperio;

    private String nombre;

    public Legado getLegado() {
        return legado;
    }

    public void setLegado(Legado legado) {
        this.legado = legado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Imperio getImperio() {
        return imperio;
    }

    public void setImperio(Imperio imperio) {
        this.imperio = imperio;
    }

    public List<Cohorte> getCohorte() {
        return cohorte;
    }

    public void setCohorte(List<Cohorte> cohorte) {
        this.cohorte = cohorte;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String name) {
        this.nombre = name;
    }

    @Override
    public String toString() {
        return "Legion{" +
                "id='" + id + '\'' +
                ", legado=" + legado +
                ", cohorte=" + cohorte +
                ", imperio=" + imperio +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
