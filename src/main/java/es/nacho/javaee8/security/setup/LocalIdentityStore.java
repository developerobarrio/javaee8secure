package es.nacho.javaee8.security.setup;

import es.nacho.javaee8.security.domain.krieg.Imperio;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.authentication.mechanism.http.BasicAuthenticationMechanismDefinition;
import javax.security.enterprise.authentication.mechanism.http.FormAuthenticationMechanismDefinition;
import javax.security.enterprise.authentication.mechanism.http.LoginToContinue;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;
import javax.security.enterprise.identitystore.IdentityStore;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

import static java.util.Arrays.asList;
import static javax.security.enterprise.identitystore.CredentialValidationResult.INVALID_RESULT;
/*
@DataSourceDefinition(
        name = "java:comp/env/jdbc/securityDS",
        className = "org.h2.jdbcx.JdbcDataSource",
        url = "jdbc:h2:~/securityTest;MODE=Oracle"
)*/
//@BasicAuthenticationMechanismDefinition(realmName = "defaultRealm")
@FormAuthenticationMechanismDefinition (
         loginToContinue = @LoginToContinue(  errorPage = "/login?error=true"  )
 )

@DatabaseIdentityStoreDefinition(
    //    dataSourceLookup = "java:comp/DefaultDataSource",
        dataSourceLookup = "java:jboss/datasources/ExampleDS",
        callerQuery = "select PASSWORD from USERS where USERNAME = ?",
         hashAlgorithm = LocalHash.class,
         groupsQuery = "select GROUPNAME from GROUPS where USERNAME = ?"

)

@ApplicationScoped
public class LocalIdentityStore {//implements IdentityStore {
//
//    private Map<String,String> users = new HashMap<>();
//
//    @PostConstruct
//    public void init(){
//        users.put("tiberio", Imperio.ROMA.name());
//        users.put("anibal", Imperio.CARTAGO.name());
//        users.put("admin", "adminadmin");
//    }
//
//    @Override
//    public CredentialValidationResult validate(Credential credential) {
//        CredentialValidationResult result = INVALID_RESULT;
//
//        UsernamePasswordCredential user = (UsernamePasswordCredential) credential;
//
//        Optional<String>  principal =  users.keySet().stream().filter(e -> user.getCaller().equalsIgnoreCase(e)).findFirst();
//        Optional<String>  password =  users.values().stream().filter(e -> user.getPasswordAsString().equalsIgnoreCase(e)).findFirst();
//
//          Boolean  condition =    principal.isPresent() && password.isPresent();
//
//          if (condition){
//                  result =  new CredentialValidationResult(principal.get(),new HashSet<>(asList(password.get())));
//          }
//
//        return result;
//    }
}