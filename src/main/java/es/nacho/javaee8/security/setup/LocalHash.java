package es.nacho.javaee8.security.setup;

import javax.security.enterprise.identitystore.PasswordHash;
import java.util.Map;

public class LocalHash implements PasswordHash {
    @Override
    public void initialize(Map<String, String> parameters) {
    }

    @Override
    public String generate(char[] chars) {
        return new String(chars);
    }

    @Override
    public boolean verify(char[] chars, String s) {
        return Boolean.TRUE;
    }
}
