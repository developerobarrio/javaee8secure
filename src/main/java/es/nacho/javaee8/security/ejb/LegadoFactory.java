package es.nacho.javaee8.security.ejb;

import es.nacho.javaee8.security.domain.krieg.Imperio;
import es.nacho.javaee8.security.domain.krieg.Legado;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class LegadoFactory {

    @PersistenceContext     EntityManager entityManager;

     public List<Legado> retrieveLegados() {
         List<Legado> resultList = entityManager.createNamedQuery(Legado.FIND_ALL, Legado.class).getResultList();
         return resultList;
    }
    public List<Legado> retrieveRomaLegados() {
        List<Legado> resultList = entityManager.createNamedQuery(Legado.FIND_ALL, Legado.class).getResultList();
        return resultList.stream().filter(e -> e.getLegion().getImperio().equals(Imperio.ROMA)).collect(Collectors.toList());
    }
    public List<Legado> retrieveCartagoLegados() {
        List<Legado> resultList = entityManager.createNamedQuery(Legado.FIND_ALL, Legado.class).getResultList();
        return resultList.stream().filter(e -> e.getLegion().getImperio().equals(Imperio.CARTAGO)).collect(Collectors.toList());
    }


}
