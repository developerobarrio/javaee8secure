package es.nacho.javaee8.security.ejb;

import es.nacho.javaee8.security.domain.krieg.Legion;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

    @Stateless
    public class LegionFactory {

        @PersistenceContext     EntityManager entityManager;

         public List<Legion> retrieveLegions() {
            return entityManager.createNamedQuery(Legion.FIND_ALL, Legion.class).getResultList();
        }
        public List<Legion> retrieveRomaLegions() {
            return entityManager.createNamedQuery(Legion.ROMANA, Legion.class).getResultList();
        }
        public List<Legion> retrieveCartagoiLegions() {
            return entityManager.createNamedQuery(Legion.CARTAGINESA, Legion.class).getResultList();
        }


}
