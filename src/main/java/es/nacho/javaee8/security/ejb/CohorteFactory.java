package es.nacho.javaee8.security.ejb;

import es.nacho.javaee8.security.domain.krieg.Cohorte;
import es.nacho.javaee8.security.domain.krieg.Imperio;
import es.nacho.javaee8.security.domain.krieg.Legion;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class CohorteFactory {

    @PersistenceContext    EntityManager entityManager;

     public List<Cohorte> retrieveCohortes() {
         TypedQuery<Cohorte> tc =  entityManager.createNamedQuery(Cohorte.FIND_ALL, Cohorte.class);

         return tc.getResultList();
    }

    public List<Cohorte> retrieveRomaCohortes() {
        List<Legion> resultList = entityManager.createNamedQuery(Legion.FIND_ALL, Legion.class).getResultList();

        return resultList.stream()
                .filter(e -> e.getImperio().equals(Imperio.ROMA))
                .map(e -> e.getCohorte())
                .flatMap(e -> e.stream())
                .collect(Collectors.toList());    }

    public List<Cohorte> retrieveCartagoCohortes() {
        List<Legion> resultList = entityManager.createNamedQuery(Legion.FIND_ALL, Legion.class).getResultList();
        return resultList.stream()
                .filter(e -> e.getImperio().equals(Imperio.CARTAGO))
                .map(e -> e.getCohorte())
                .flatMap(e -> e.stream())
                .collect(Collectors.toList());

    }


}
