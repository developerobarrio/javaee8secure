package es.nacho.javaee8.security.rest;

import es.nacho.javaee8.security.domain.krieg.Cohorte;
import es.nacho.javaee8.security.ejb.CohorteFactory;
import es.nacho.javaee8.security.setup.Secure;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("cohorte")
public class CohorteResource {

    @Inject private CohorteFactory  cohorteFactory;

    @GET
    @RolesAllowed({"administrator"})
    @Secure
    @Produces(    MediaType.APPLICATION_JSON)
       public String get(){

        List<Cohorte> cohortes = cohorteFactory.retrieveCohortes();
        Jsonb jsonb = JsonbBuilder.create();
        String result = jsonb.toJson(cohortes);
        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"ROMA","administrator"})
    @Path("roma")
    @Secure
    public String getRoma()
    {
        List<Cohorte> cohortes = cohorteFactory.retrieveRomaCohortes();
        Jsonb jsonb = JsonbBuilder.create();
        String result = jsonb.toJson(cohortes);
        return result;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"CARTAGO","administrator"})
    @Path("cartago")
    @Secure
    public String getCartago(){
        List<Cohorte> cohortes = cohorteFactory.retrieveCartagoCohortes();
        Jsonb jsonb = JsonbBuilder.create();
        String result = jsonb.toJson(cohortes);
        return result;
    }

}
