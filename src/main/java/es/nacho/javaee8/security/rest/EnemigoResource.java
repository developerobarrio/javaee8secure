package es.nacho.javaee8.security.rest;

import es.nacho.javaee8.security.domain.krieg.Imperio;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Optional;


@Path("enemigo")
public class EnemigoResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getImperio(@Context HttpServletRequest request) {

        String result="";

        Optional<Imperio> role = Arrays.asList(Imperio.values())
                .stream()
                .filter(e -> !request.isUserInRole(e.name()))
                .findFirst();

        if (role.isPresent()  && (!request.isUserInRole("administrator"))) {
            result = role.get().name().toLowerCase();
        }

        return result;
    }


}
