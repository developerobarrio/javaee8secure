package es.nacho.javaee8.security.rest;

import es.nacho.javaee8.security.domain.krieg.Legion;
import es.nacho.javaee8.security.ejb.LegionFactory;
import es.nacho.javaee8.security.setup.Secure;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.security.enterprise.SecurityContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("legion")
public class LegionResource {

    @Inject private LegionFactory legionFactory;
    //@Inject SecurityContext sc;
    @GET
    @Produces(    MediaType.APPLICATION_JSON)
    @RolesAllowed({"administrator"})
    @Secure
    public String get(){

        List<Legion> legion = legionFactory.retrieveLegions();
        Jsonb jsonb = JsonbBuilder.create();
        String result = jsonb.toJson(legion);
        return result;

    }

    @Override
    public String toString() {
        return super.toString();
    }

    @GET
    @Produces(    MediaType.APPLICATION_JSON)
    @RolesAllowed({"CARTAGO","administrator"})
    @Path("cartago")
    @Secure
    public String getCartago()
    {
        List<Legion> legion = legionFactory.retrieveCartagoiLegions();
        Jsonb jsonb = JsonbBuilder.create();
        String result = jsonb.toJson(legion);
        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"ROMA","administrator"})
    @Path("roma")
    @Secure
    public String getRoma(){
        List<Legion> legion = legionFactory.retrieveRomaLegions();
        Jsonb jsonb = JsonbBuilder.create();
        String result = jsonb.toJson(legion);
        return result;
    }

}
