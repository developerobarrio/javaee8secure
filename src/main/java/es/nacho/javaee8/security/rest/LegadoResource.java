package es.nacho.javaee8.security.rest;

import es.nacho.javaee8.security.domain.krieg.Legado;
import es.nacho.javaee8.security.ejb.LegadoFactory;
import es.nacho.javaee8.security.setup.Secure;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("legado")
public class LegadoResource {

    @Inject private LegadoFactory legadoFactory;

    @GET
    @RolesAllowed({"administrator"})
    @Secure
    @Produces(   MediaType.APPLICATION_JSON)
       public String get(){

        List<Legado> legados = legadoFactory.retrieveLegados();
        Jsonb jsonb = JsonbBuilder.create();
        String result = jsonb.toJson(legados);
        return result;
    }


    @GET
    @Produces(    MediaType.APPLICATION_JSON)
    @RolesAllowed({"CARTAGO","administrator"})
    @Path("cartago")
    @Secure
    public String getCartago(){
        List<Legado> legados = legadoFactory.retrieveCartagoLegados();
        Jsonb jsonb = JsonbBuilder.create();
        String result = jsonb.toJson(legados);
        return result;
    }

    @GET
    @Produces(    MediaType.APPLICATION_JSON)
    @RolesAllowed({"ROMA","administrator"})
    @Path("roma")
    @Secure
    public String getRoma(){
        List<Legado> legados = legadoFactory.retrieveRomaLegados();
        Jsonb jsonb = JsonbBuilder.create();
        String result = jsonb.toJson(legados);
        return result;
    }

}
